class Color {

  constructor (color){
    let rgbFormat = /[0-9]{1,3},[ ]*[0-9]{1,3},[ ]*[0-9]{1,3}/g
    let hexFormat = /#[0-9a-fA-F]{3,6}/g
    if (color.match(hexFormat)) {
      this._hex = color
      this._rgb = this._hexaToRgb(color)
    } else if (color.match(rgbFormat)) {
      this._hex = this._rgbToHexa(color)
      this._rgb = color
    } else {
      this._hex = "#000"
      this._rgb = "0, 0, 0"
    }
  }

  get hex () { return this._hex }
  get rgb () { return this._rgb }

  _hexaToRgb (hexa) {
    let hex = hexa.replace('#','');
    let r = parseInt(hex.substring(0,2), 16);
    let g = parseInt(hex.substring(2,4), 16);
    let b = parseInt(hex.substring(4,6), 16);
    return r+', '+g+', '+b;
  }

  _compToHex(comp) {
    var hex = Number(comp).toString(16);
    if (hex.length < 2) {
         hex = "0" + hex;
    }
    return hex;
  }

  _rgbToHexa(rgb) {
    let trgb = rgb.split(',')
    return "#"
    + this._compToHex(parseInt(trgb[0]))
    + this._compToHex(parseInt(trgb[1]))
    + this._compToHex(parseInt(trgb[2]))
  }

}
