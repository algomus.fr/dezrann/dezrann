  class SnapText {

    constructor (textView, marginTop) {
        this._textView = textView;
        this._marginTop = marginTop;
        // when empty, snap.paper.text is an empy array ([]) instead of ""...
        let text = this._textView.attr("text")
        this._fullText = text.length == 0 ? "" : text
    }

    get text () {
        return this._fullText
    }

    set text (text) {
      this._fullText = text
      this._textView.attr({"text": this._fullText});
    }

    set width (width) {
      this._textView.attr({"text": this._fullText})
      let box = this.box;
      let i = 1
      const MIN_CHARS_TO_DISPLAYED = 5
      while (box.width + 10 > width && i < this._fullText.length - MIN_CHARS_TO_DISPLAYED) {
        this._textView.attr({"text": this._fullText.substring(0,this._fullText.length-i) + "..."})
        box.text = this._textView
        i++
      }
      if (box.width + 10 > width && i >= this._fullText.length - MIN_CHARS_TO_DISPLAYED) {
        this._textView.attr({"text": ""})
      }
    }

    get box() {
      return new Box(this._textView);
    }

    moveTo (x, y, width) {
      this._textView.attr({"x": x, "y": y + this._marginTop})
    }

    remove () {
        this._textView.remove();
    }

}

class Box {

    constructor (text) {
      this.text = text
    }

    set text (value) {
      let bbox = value.getBBox()
      this._width = bbox.x2 - bbox.x
      this._height = bbox.y2 - bbox.y
    }

    get width () { return this._width }
    get height () { return this._height }

}
