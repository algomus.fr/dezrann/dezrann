var epsilon = 0.01 ;
var reMeasureFrac = /^(\d+[.]?\d*)?(([+-]?\d+)\/(\d+))?$/ ;

function aboutZero(x) {
    return (Math.abs(x) < epsilon);
}

function parseDurationMeasureFrac(str = '0', timeSigNum = 4, timeSigDenum = 4) {
    /**
     * Parse a string representing an offset (in measures) into a float (in quarters)
     * 1/4 should be always a quarter, even in 2/4 or 3/4 measures
     * @param {string} str - string representation such as '14', '14+1/4' or '15-1/8'
     * @param {int} quartersByMeasure - 4 for 4/4
     * @return {float} measure - float offset such as 52.0, 53.0 or 55.5
     */

    var timeSig = timeSigNum / timeSigDenum
    var match = str.match(reMeasureFrac);

    if (!match)
        throw "BadFractionalString";

    var measure = match[1] == undefined ? 0 : parseFloat(match[1]);

    if (typeof match[3] !== 'undefined')
        measure += parseInt(match[3]) / (parseInt(match[4]) * timeSig)

    return measure * timeSig * 4
}


function toDurationMeasureFrac(offset = 0, timeSigNum = 4, timeSigDenum = 4, round = false, isDuration = true) {
    /**
     * Convert a float (in quarters) to a string representing an offset (in measures)
     * 1/4 should be always a quarter, even in 2/4 or 3/4 measures
     * @param {float} measure - float offset such as 52.0, 53.0 or 55.5
     * @param {int} timeSigNum - 4 for 4/4
     * @param {bool} round - round (actually ceil) to an integer
     * @return {string} str - string representation such as '14', '14+1/4' or '15-1/8'
     */

    var timeSig = timeSigNum / timeSigDenum
    var offset4 = offset / (timeSig * 4)
    var measure = Math.floor(offset4 + epsilon)
    var frac = (offset4 - measure) * timeSig // can be >= 1.0 for non x/4 time signatures

    if (aboutZero(frac) || round)
        return measure.toString()

    for (var denum of [4,8,16,32, 12,24,48]) {
        if (timeSigDenum > denum)
            continue

        var num = frac * denum
        var numRound = Math.floor(num + 0.01)
        if (aboutZero(num - numRound)) {
            if (((measure == 0) && !isDuration) || (frac > 0.874))
                // upbeat
                return (measure + 1) + '-' + (denum  * timeSig - numRound) + '/' + denum
            else
                // regular postion
                return (measure == 0 ? '' : (measure + '+')) + numRound + '/' + denum ;
            }
    }
    return (offset / timeSigNum).toString()
}


function parseMeasureFrac(str, timeSigNum = 4, timeSigDenum = 4, upbeat = 0) {
    return parseDurationMeasureFrac(str, timeSigNum, timeSigDenum) - 4*timeSigNum/timeSigDenum + upbeat
}

function toMeasureFrac(offset, timeSigNum = 4, timeSigDenum = 4, upbeat = 0) {
    return toDurationMeasureFrac(offset + 4*timeSigNum/timeSigDenum - upbeat, timeSigNum, timeSigDenum, false, false)
}

function toMeasureRounded(offset, timeSigNum = 4, timeSigDenum = 4, upbeat = 0) {
    return toDurationMeasureFrac(offset + 4*timeSigNum/timeSigDenum - upbeat, timeSigNum, timeSigDenum, true, false)
}



// From underscore.js
function debounce(func, wait, immediate) {
    var timeout, args, context, timestamp, result;

    var later = function() {
        var last = Date.now() - timestamp;

        if (last < wait && last >= 0) {
            timeout = setTimeout(later, wait - last);
        } else {
            timeout = null;
            if (!immediate) {
                result = func.apply(context, args);
                if (!timeout) context = args = null;
            }
        }
    };

    return function() {
        context = this;
        args = arguments;
        timestamp = Date.now();
        var callNow = immediate && !timeout;
        if (!timeout) timeout = setTimeout(later, wait);
        if (callNow) {
            result = func.apply(context, args);
            context = args = null;
        }

        return result;
    };
}
