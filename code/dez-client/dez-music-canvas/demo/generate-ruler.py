
import json



j = {}
j['labels'] = []

for i in range(0, 26):
    j['labels'].append({"start": 4*i, "tag": '%d' % (i+1),  "duration": 4,  "type": "S",  "staff": 2 + (i%2) })
    if (i % 10) == 0:
        j['labels'].append({"start": 4*i, "tag": '%d' % (i+1),  "duration": 5*4,  "type": "CS1",  "staff": 1 })

    if (i % 5) == 0:
        j['labels'].append({"start": 4*i, "tag": '%d' % (i+1),  "duration": 0,  "type": "Cadence" })


j['labels'].append({"start": 0, "tag": 'music (25 measures)',  "duration": 25*4, "type": "Structure"  })
        
open('./ruler-100.dez', 'w').write(json.dumps(j))
