# Dezrann authentication service

## Local install

### generate RSA keys

```
openssl genpkey -algorithm RSA -out private_key.pem -pkeyopt rsa_keygen_bits:2048
openssl rsa -in private_key.pem -pubout -out public_key.pem
```

You have to copy the `public_key.pem` in the `dezrann-back/config` directory too to be able to uncrypt jwt.

### start the local service

In `code/dez-server/``

- create `config.json` by copying `config-sample.json`

      cp config-sample.json config.json

- run `npm start`
- the server now answer, as for example <http://localhost:9999/ping   
If your browser returns "pong", the authentication service is working
- you can log in as the 'adminitrator' with password 'secret' (with salt.[prefix|suffix] as in `config-sample.json`).

### Add a new user

Edit `users.json` file to add an entry:

```
"newuser" : {
  "uid" : "newuser",
  "password" : "91b5752df992e7f3975ede8465a13a2ae704485ea2e17fe561c0a47a8a7c0f55"
}

```

Password is 'secret'. If you want another password replace the value of `password` key by the result of this command:

```
$ echo -n "salt prefixstrongsecretsalt suffix" | openssl dgst -sha256
SHA2-256(stdin)= ce318d0f7a5bfd6bf8a4e19fd3011f901fcebe11c3b6d7582b57bd21fde06bfc
```

`ce318d0f7a5bfd6bf8a4e19fd3011f901fcebe11c3b6d7582b57bd21fde06bfc` is the hash of `strongsecret`.

## Deploy to production

Deploying the service is a work in progress. The best way is to have a server with sshd and [pm2](https://pm2.keymetrics.io/) installed.

### Build

```
./bin/build.sh
```

### Deploy with ansible

You have to install ansible (`sudo apt-get install ansible` on debian/ubuntu)

Edit a `hosts.yml`:

```
[auth]
${auth-server-fqdn-or-ip} ansible_user=${ansible_user}
```

Deploy!

```
./bin/deploy.sh
```