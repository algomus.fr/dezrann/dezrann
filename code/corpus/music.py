
import music21

def two_voices_per_staff(score):
    '''returns another music21 score where voices are grouped two by two.'''
    print('    partsToVoices')

    # return score.partsToVoices(2) # strange things on first incomplete measures
    s = music21.stream.Score()

    n = len(score.parts)
    for i in range(n):
        if i % 2 == 0:
            p = music21.stream.Part()

        v = music21.stream.Voice()
        for m in score.parts[i]:
            # set the stems in the measure
            try:
                for e in m:
                    if hasattr(e, 'stemDirection'):
                        e.stemDirection = ['up', 'down'][i % 2]
            except:
                pass

            # insert the measure
            v.insert(m)
        p.insert(v)

        if i % 2 == 1 or i == n -1:
            s.insert(p)

    s.makeMeasures()
    return s


def music_to_lilypond(f, ff, infos, musicxml2ly=False):
    print('<== ', f)
    try:
        music21.humdrum.spineParser.flavors['JRP'] = True
        c = music21.converter.parse(f)
    except Exception as e:
        print('! Error: ', e)
        return None

    # Voices per staff
    if 'voices-per-staff' in infos:
        if infos['voices-per-staff'] == 2 and len(c.parts) == 4:
            try:
                cc = two_voices_per_staff(c)                
                c = cc
            except Exception as e:
                print('! Error: ', e)
        else:
            del infos['voices-per-staff']
            c = None

    if c is None:
        print('==  skipped')
        return None

    # Time signature and upbeat
    try:
        sop = c.parts[0]
        ts = sop.recurse().getElementsByClass(music21.meter.TimeSignature)[0]
        infos['time-signature'] = ts.ratioString
        mes = sop.recurse(streamsOnly=True).getElementsByClass('Measure')[:2]
        if len(mes) == 2:
            m0d = mes[0].duration.quarterLength
            m1d = mes[1].duration.quarterLength
            if (m0d < m1d):
                infos['time-signature-upbeat'] = m0d
    except Exception as e:
        print("! Error in fetching time signature information: %s" % e)

    # Lilypond output
    print('==> ', ff, infos)
    try:
        if not musicxml2ly :
            remove_no_chords(c).write('lily', ff)
        else :
            musicxml_to_lilypond(f, ff)
        return True
    except Exception as e:
        print('! Error: %s: %s' % (ff, e))
        return None


def musicxml_to_lilypond(f_in, f_lily):
    cmd = ["musicxml2ly", "--npl", f_in, "-o", f_lily]
    import subprocess
    subprocess.check_output(cmd)

def remove_no_chords(s):
    remove_list = []
    for el in s.recurse():
        if isinstance(el, music21.harmony.NoChord):
            remove_list.append(el)
    s.remove(remove_list, recurse=True)
    return s
