'''
Compress lilypond .png by using positions.json, and rewrites positions.jso
python3 compress.py ../../corpus/*/*png
'''

from PIL import Image
import json
import os.path
import sys

PAD_MIN = 5

def compress_tops_bots(tb):
    last_b = -PAD_MIN
    tb_new = []
    shift = 0
    
    for (t, b) in tb:
        t_new = min(t+shift, last_b + PAD_MIN)
        shift = t_new - t
        b_new = b + shift
        tb_new.append((t_new, b_new))
        last_b = b_new

    return tb_new
        
def compress(f, f_pos, ff, ff_pos, staffTopMargin=0, staffBottomMargin=0):

    # Read and compress (top, bottom) pairs
    print('<==', f_pos)
    pos = json.load(open(f_pos))
    tb = [(int(s['top']) - staffTopMargin, int(s['bottom']+1) + staffBottomMargin) for s in pos['staffs']]
    tb.sort()
    print('   ', tb)
    tb_new = compress_tops_bots(tb)
    print('   ', tb_new)
    h_new = int(tb_new[-1][1])

    # Image manipulation
    print('<==', f)
    im = Image.open(f)
    im_new = Image.new(im.mode, (im.width, h_new))

    # Prepare background (white + barlines)
    for i in range(len(tb)-1):
        h = tb_new[i+1][0] - tb_new[i][1]
        box = (0, tb[i][1], im.width, tb[i][1]+h)
        box_new = (0, tb_new[i][1], im.width, tb_new[i][1] + h)
        region = im.crop(box)
        im_new.paste(region, box_new)

    # Copy staves
    for i in range(len(tb)):
        box = (0, tb[i][0], im.width, tb[i][1])
        box_new = (0, tb_new[i][0], im.width, tb_new[i][1])

        # print('%s -> %s' % (box, box_new))
        region = im.crop(box)
        im_new.paste(region, box_new)

    print('==>', ff)
    im_new.save(ff)

    # Save new positions
    # print(pos['staffs'])
    
    pos['staffs'] = [ { 'top': t, 'bottom': float(b) - .1, 'notespos': [] } for (t, b) in tb_new ]
    # print(pos['staffs'])
    print('==>', ff_pos)
    open(ff_pos, 'w').write(json.dumps(pos, sort_keys=True, indent=4))


def compress_piece(f, staffTopMargin=0, staffBottomMargin=0):

    f_pos = os.path.dirname(f) + '/positions.json'
    ff = f + '.new.png'
    ff_pos = f_pos + '.new'
    compress(f, f_pos, ff, ff_pos, staffTopMargin, staffBottomMargin)

    os.system('mv -v %s %s' % (ff, f))
    os.system('mv -v %s %s' % (ff_pos, f_pos))

# compress_piece('../../corpus/bwv253/bwv253.png')


if __name__ == '__main__':
    for f in sys.argv[1:]:
        try:
            compress_piece(f)
        except Exception as e:
            print("!", e)
        print()

