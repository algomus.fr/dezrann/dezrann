
from PIL import Image, ImageFont, ImageDraw, ImageFilter

import json
import re
import sys
import os

RE_TIME = re.compile('(\d*):(\d\d)([.]\d)?')

if len(sys.argv) > 1:
    ID = sys.argv[1]
else:
    ID = 'stand-by-me'


DIR_IN = './data/edmus/%s/' % ID
LYRICS = DIR_IN + '%s.lyrics' % ID

DIR_OUT = '../../corpus/%s/' % ID
os.system('mkdir -p %s' % DIR_OUT)

IMAGE = DIR_OUT + '%s-lyrics.png' % ID
SYNCHRO = DIR_OUT + '%s-lyrics.json' % ID

def json_synchro(raw_synchro):
    j = json.loads('{}')
    j['date-x'] = [{ "x": x, "date": time } for (x, time) in raw_synchro]
    j['staffs'] = [ { "bottom": 95.0,  "top": 5.0 },
                    { "bottom": 195.0, "top": 105.0 },
                    { "bottom": 295.0, "top": 205.0 } ]
    return j

def iter_lytime_from_lyrics(lyrics, start=0):
    '''
    >>> list(lyrics_to_lytime(['bla', '0:03', '1:05', 'bli', 'blu', '1:07'], start=None))
    [(0, 3, 'bla'), (65, 67, 'bli blu')]
    '''

    if start is None:
        previous_time = 0
    else:
        yield(start, start, '')
        previous_time = start

    current_lyrics = []
    
    for l in lyrics:
        m = RE_TIME.match(l)
        if m:
            # Time information
            time = int(m.group(1))*60 + int(m.group(2))
            if m.group(3):
                time += float(m.group(3)[1:]) / 10

            if current_lyrics:
                yield (previous_time, time, ' '.join(current_lyrics))
                current_lyrics = []
            previous_time = time
        else:
            # Lyric word
            current_lyrics.append(l)
            

def lyrics_to_image(lyrics):

    w = 20000
    
    im = Image.new('RGB', (w, 300), (255, 255, 255))  
    draw = ImageDraw.Draw(im)

    font = ImageFont.truetype("Raleway-Regular.ttf", 80)

    last_time = 0
    current_x = 50
    y = 150
    (ws, hs) = font.getsize(' ')

    synchro_raw = []
    
    for (time_a, time_b, lyrics) in iter_lytime_from_lyrics(lyrics):

        print('    "%s"' % lyrics, lyrics.endswith('-'))
        if lyrics.endswith('-'):
            lyrics = lyrics[:-1]
            contin = True
        else:
            contin = False
        
        if time_a > last_time:
            current_x += min((time_a - last_time), 5) * 5 * ws
        
        (w, h) = font.getsize(lyrics)
        draw.text((current_x, y), lyrics, font=font, fill=(0, 0, 0))

        synchro_raw.append((int(current_x), time_a))
        current_x += w 
        synchro_raw.append((int(current_x), time_b))
        if not contin:
            current_x += ws

        last_time = time_b

    blur = im.filter(ImageFilter.BLUR)
    blur.putalpha(64)
    im.paste(blur, (0, 0), blur)

    return(im, json_synchro(synchro_raw))

    

if __name__ == '__main__':
    print('<-- ' + LYRICS)
    l = ' '.join(open(LYRICS).readlines()).split()

    (im, sync) = lyrics_to_image(l)

    print('--> ' + IMAGE)
    im.save(IMAGE, "PNG")

    print('--> ' + SYNCHRO)
    json.dump(sync, open(SYNCHRO, 'w'), indent=2)
