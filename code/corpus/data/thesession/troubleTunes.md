# Troubles on tunes inventory

## Bad abc

### pb anacrousis

- http://www.dezrann.net/~/the-session/7-bonaparte-crossing-the-rhine
- http://www.dezrann.net/~/the-session/49-harvest-home

### répétitions

- http://www.dezrann.net/~/the-session/8-banshee
- http://www.dezrann.net/~/the-session/5-blarney-pilgrim
- http://www.dezrann.net/~/the-session/9-banish-misfortune
- http://www.dezrann.net/~/the-session/10-butterfly
- http://www.dezrann.net/~/the-session/12-cliffs-of-moher
- http://www.dezrann.net/~/the-session/20-cup-of-tea
- http://www.dezrann.net/~/the-session/27-drowsy-maggie
- http://www.dezrann.net/~/the-session/30-off-to-california
- http://www.dezrann.net/~/the-session/31-franc-a-phoill
- http://www.dezrann.net/~/the-session/33-farewell-to-ireland
- http://www.dezrann.net/~/the-session/34-frieze-breeches
- http://www.dezrann.net/~/the-session/38-galway
- http://www.dezrann.net/~/the-session/48-hardiman-the-fiddler
- http://www.dezrann.net/~/the-session/52-kid-on-the-mountain
- http://www.dezrann.net/~/the-session/55-kesh
- http://www.dezrann.net/~/the-session/60-lilting-banshee

#### reprises alternatives (1, 2)

- http://www.dezrann.net/~/the-session/26-donnybrook-fair

### croches transformées en doubles??

- http://www.dezrann.net/~/the-session/39-kerry

### triolets mal gérés

- http://www.dezrann.net/~/the-session/42-gravel-walks
- http://www.dezrann.net/~/the-session/49-harvest-home

## Not Found

- Haul her Across the Road (jig)
- Humours of california (hornpipe)
- Jackie Tar
- Na Ceannablan Blana
- O’Keefe’s (slide)
- Old Mountain Road
- Paddy's return
- Pipes on The Hob
- Roll out the barrel (reel)
- Sean Reid's Favourite
- She Beag She Mor
- The blackbird (reel)
- The Blackbird Hornpipe
- the fairies' hornpipe (hornpipe)
- The Lark in the Morning
- The Man of Aran
- The montain road (reel)
- The Sailor's Return (reel)
- The Virginia :
- The Wild Irishman
- The wise maid (reel)
- Tom Busby's
- Will You Come Home With Me?

