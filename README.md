
# Dezrann

[Dezrann](www.dezrann.net) is an open platform to hear, study, and annotate music on the web.
It is employed to share and analyze music in the form of scores, images, audio/video/waveforms, and annotations, with cross-modal synchronization. 
Dezrann's applications span *music education*, as well as corpus annotation and interaction for *research in musicology and computer music*. 

The public beta server is available from <http://www.dezrann.net>.

The code is hosted on <https://gitlab.com/algomus.fr/dezrann>, in particular in these three active repositories:
- <https://gitlab.com/algomus.fr/dezrann/dezrann-front>: front (TypeScript, Vue.js)
- <https://gitlab.com/algomus.fr/dezrann/dezrann-back>: back (TypeScript, Node.js)
- <https://gitlab.com/algomus.fr/dezrann/dezrann-corpus>: corpus data and metadata (json, Python)

Dezrann is open-source, licensed under the GPL version 3 or any later version,
and is developped by 
Emmanuel Leguy, Mathieu Giraud, Lou Garczynski, Charles Ballester, 
with the help of many [contributors](AUTHORS.md) 
in the Algomus computer music team and elsewhere.

## Contact

Contact us at <contact@dezrann.net>.

## References

See [References](AUTHORS.md#references).