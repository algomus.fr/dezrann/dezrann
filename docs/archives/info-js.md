
```javascript

{
    "composer": "J.-S. Bach",
    "id": "bwv847",
    "time-signature": "4/4",
    "title": "Fuga #02 in C minor, BWV 847",
    "title:fr": "Fugue n\u00b002 en do mineur, BWV 847"
    
    "sources": {
        "audios": [
            {
                "file": "bwv847.mp3",
                "onset-date": "synchro.json",
                "info": "Recording by Xxxx",
                
                "images": [
                    {
                        "image": "bwv847-waves.png",
                        "positions": "bwv847-waves-pos.json",
                        "type": "wave"
                    },
                    {
                        "image": "bwv847-spec.png",
                        "positions": "bwv847-spec-pos.json",
                        "info": "blabla",
                        "type": "wave",
                        "name": "spectrogram"
                    }
                ]
            },
            {
                "file": "bach-02-another-recording.mp3",
                "onset-date": "...",
                "info": "Recording by Yyyyyyyy",
                "images": [ ... ],
            },            
        ],
        "images": [
            {
                "image": "bwv847.png",
                "positions": "positions.json",
                "type": "score",
                "info": "Produced by Lilypond from KernScore"
            },
            {
                "image": "bwv847-autograph.jpg",
                "positions": "positions-autograph.json",
                "type": "score",
                "name": "autograph"
            },
            {
                "type": "score-svg",
                "image": "bwv847-bla.svg" 
            }
        ]
    },

    // à discuter
    {"type": "video", "video": "xxxx.mp4", "synchro": "xxx"},
    
    {"type": "slideshow", 
     "images": [ "xxxx", "xxx", "xxx"],
     "synchro": "xxx"},
    
    {"type": "lyrics",  "lyrics": "xxx format à voir, avec synchro ? xxx"}
}
