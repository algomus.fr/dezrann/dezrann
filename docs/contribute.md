
# How to Contribute

Many [people](contributors) contribute to Dezrann.
We welcome contributions from anyone interested in improving Dezrann. Whether you’re a musicologist, a developer, or someone passionate about open music data, there are many ways to get involved!

Feel free to contact us at [contact@dezrann.net](mailto:contact@dezrann.net) to discuss any of the points below or other ideas you may have.

## Use the Platform for Your Pleasure and/or Research

The simplest way to contribute is to use and to promote the platform: Enjoy discovering new music and their analyses!

If you work in MIR or musicology research, we’d be happy to discuss how Dezrann can support your projects.  
- As a musicologist, you can host your own corpus on Dezrann (see below).  
- As an MIR researcher, your algorithms can produce data, analyses, or annotations that can be directly visualized on Dezrann.


## Report Bugs and Issues

If you encounter bugs or issues on the platform, let us know!  
- The preferred method is to open issues directly on the relevant GitLab tracker: [GitLab Issues](https://gitlab.com/algomus.fr/dezrann/issues).  
- If you don’t have a GitLab account, you can email us at [contact@dezrann.net](mailto:contact@dezrann.net).  

When reporting an issue, please include as much detail as possible, including steps to reproduce the problem and screenshots, if applicable.


## Synchronization

Synchronized audio/video sources are essential for enhancing the experience of using Dezrann. While we are working on semi-automatic synchronization, as of 2025, most synchronization is manually curated.  

- The next **synchronization party** is planned for **[XXXXXX] 2025**. Join us for a collaborative effort to improve synchronization across corpora.  
- In the meantime, feel free to contact us if you’d like to help synchronize specific pieces or datasets.


## Code Contributions

If you’d like to contribute to the frontend, backend, or corpus-related code, we’d be happy to add you to the GitLab project. Feel free to create a branch and submit a merge request (MR). Ensure that your changes are well-documented.

### Frontend (UI/UX)

Our frontend is built using **TypeScript** and **Vue.js**.  
- Repository: [Frontend Repository](https://gitlab.com/algomus.fr/dezrann/dezrann-front).  
- Contributions to improve the user interface, performance, and accessibility are highly valued.

### Backend (Data and Processing)

Our backend manages data integration, synchronization, and processing.  
- Repository: [Backend Repository](https://gitlab.com/algomus.fr/dezrann/dezrann-back).  
- Contributions to enhance functionality, stability, and performance are greatly appreciated.


## Corpus Contributions

### Technical Integration

Help us improve the technical integration of existing corpora.  
- Visit the [Status Page](https://doc.dezrann.net/status) to see the current state of corpora, including synchronization, metadata, and quality ratings.  
- Identify issues, report them, and work on resolving them.

### Corpus Curation

Enhance the quality of existing datasets:  
- Review each piece in a corpus and improve its quality measures (see [Quality Documentation](https://doc.dezrann.net/quality)).  
- Suggest or implement improvements to ensure the datasets are more robust and complete.

### Propose a New Corpus

We are committed to increasing the diversity of corpora on Dezrann.  
- See the guidelines: [Corpus Contribution Guide](https://gitlab.com/algomus.fr/dezrann/dezrann-corpus/-/blob/main/README.md).
- Ensure that your corpus aligns with our mission of promoting diversity and openness.  
- Contact us to collaborate on integrating your corpus.


## Teaching or Tutorial Material

Dezrann is also a valuable resource for education.  
- As of 2025, there are 8 "fiches pédagogiques" (teaching sheets) in French for secondary schools, soon available at <https://algomus.fr/edmus/>.  
- We welcome new educational content or adaptations in other languages.


## Find or Provide New Open Data

Open data is crucial for music and MIR research. Read how we handle [open licenses](licenses) on the Dezrann platform.

Do you have open datasets with scores, analyses, or audio/videos? Are you planning to create or record such data? Do you know people who own interesting data and could work with them to release it under an open license?

We encourage submissions of high-quality, openly licensed data to enrich the platform and benefit everyone.  
Contact us to discuss how we can collaborate to make more open music data available!

## Get Involved

If you have other ideas or ways to contribute, don’t hesitate to reach out! Dezrann is a collaborative effort, and we’re excited to work with contributors from all backgrounds.

Contact us at [contact@dezrann.net](mailto:contact@dezrann.net) for more information.

Thank you for helping make Dezrann better and for sharing open music data!
