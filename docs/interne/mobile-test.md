# Tests sur mobile (sous chrome)

## Simulation

Ouvrir l'outil de développement Chrome (Crtl+Alt+i) et cliquer sur le bouton
'toggle device toolbar'. Il est possible ensuite de sélectionner plusieurs
modèles de téléphone ou tablettes.

https://developers.google.com/web/tools/chrome-devtools/device-mode/

## Utiliser l'outil de dev sur mobile

Sous android, l'outil de dev de chrome n'existe pas. Cependant, il est possible
de débugger en connectant l'appareil en usb sur un ordinateur faisant tourner
chrome. L'instance de chrome sur l'ordinateur peut alors inspecter l'instance
de chrome sur la tablette.

Tout est expliqué ici:

https://developers.google.com/web/tools/chrome-devtools/remote-debugging/

En résumé:

- sur la tablette/téléphone il faut activer le mode développeur:
  - aller dans paramètres
  - sélectionner 'à propos de ...'
  - taper 7 fois sur 'numero de build'
  - le mode dev est alors activé, l'item 'options pour les développeurs'
  s'affiche dans les paramêtres
- dans 'options pour les développeurs' activer 'debuggage usb'
- dans l'outil de dev de l'instance chrome de l'ordi:
  - dans le menu ('...' horizontaux) sélectionner 'More tools / remote devices'.
  - l’appareil connecté en usb devrait s'afficher dans 'remote devices'.
  Sélectionner le.
  - les urls courantes de l'instance de chrome de la tablette s'affichent. Si
  celle qui vous intéresse n'est pas listée, ajoutez la.
  - appuyer sur le bouton 'inspect' devantb l'url qui vous intéresse,
  l'inspecteur s'ouvre avec l'affichage de la tablette. Vous pouvez manipuler la
  webapp soit sur la tablette soit sur l'ordi (via l'inspecteur). Vous
  pouvez alors visualiser la console ou le debugger.


## Utiliser un (ou plusieurs) serveur local à la machine de dev

Il est possible de tester sur une tablette tout serveur qui tourne sur votre
machine de dev :
- une webapp (polymer serve)
- un webservice
- tout autre serveur

Cela se fait comme dans la partie précedente en activant le mode dev et en
connectant la tablette et l'ordi de dev en usb.

Ensuite dans l'onglet 'remote device' (intance de chrome sur l'ordi) il faut
activer la redirection de port.

Il suffit ensuite d'ajouter les redirections (add rule). Par exemple si le
serveur local écoute sur le port 8081, choisissez par exemple 3333 comme
'device port' et saisissez '127.0.0.1:8081' comme 'local address'.

Votre serveur est alors accessible depuis votre tablette avec chrome à
l'adresse 'http://localhost:3333'.

Exemple pour accéder à dezrann:
- le web service:
  - device port: 8888
  - local address: 127.0.0.1:8888
  - url d'accès: http://localhost:8888
- le client (polymer serve -p 8081)
  - device port : 8081
  - local adress: 127.0.0.1:8081
  - url d'accès: http://localhost:8081

Pour en savoir plus:

https://developers.google.com/web/tools/chrome-devtools/remote-debugging/local-server
