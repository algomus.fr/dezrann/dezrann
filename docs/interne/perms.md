
# Gestion des permissions dans Dezrann

Disclaimer: doc en cours de rédaction et de validation. Des choses restent à implémenter.

## 1 - Principe général

La gestion des permissions est centrée sur les ressources à protéger (corpus, pièces, analyses...). A chaque ressource correspondent des permissions (CRUD) pour des sujets (utilisateurs ou groupes). Lors du contrôle d'accès, il suffit de vérifier si le sujet a le type d'accès correct à la ressource.

## 2 - Ressources

Les ressources sont organisées de manière hierarchique. Il y a des ressources contenantes (noeuds) et des ressources contenues.

Les ressources contenantes sont:
- les corpus qui contiennent un ensemble de pièces et/ou des sous-corpus
- les pièces qui contiennent des analyses et des sources (images, audios)

Les pièces et les corpus peuvent être contenus dans des corpus.

Les pièces contiennent des ressources feuilles:
- les analyses
- les images représentant la pièce (partition, forme d'onde...)
- les audios

### 2.1 - Besoin de graphe de ressource

L'organisation hiérarchique est une première approximation. Par la suite nous aurons peut-être des cas d'utilisation nécessitant qu'une ressource (par exemple une pièce) soit contenue dans plusieurs ressources contenantes (par exemple, plusieurs corpus différents).

## 3 - Sujets: comptes utilisateurs et groupes

Un sujet est soit un utilisateur soit un groupe.

### 3.1 - Comptes utilisateurs

Pour accéder à des ressources protégées, un utilisateur doit avoir un compte pour s'identifier et s'authentifier à l'application. Ce compte comporte un identifiant (login ou uid) et un mot de passe.

### 3.2 - Groupes d'utilisateurs

Un groupe est une liste de sujets, c'est à dire une liste d'utilisateurs et/ou de groupes d'utilisateurs.

### 3.3 - Sujets spéciaux

#### 3.3.1 - public

Lorsqu'un utilisateur n'est pas authentifié aucun compte n'est utilisé. Le sujet utilisé lors du contrôle d'accès est 'public'.

#### 3.3.2 - admin

Il a tous les droits sur toutes les ressources. Il peut également créer les comptes et les groupes d'utilisateurs. Il peut également donner les permissions d'accès (GRANT) à toutes les ressources pour tous les utilisateurs.

##### 3.3.2.1 - Administrateur de corpus et de groupe (non implémenté)

Un utilisateur peut avoir le rôle d'administrateur pour un corpus. Il a alors tous les droits sur toutes les ressources du corpus. Il a le pouvoir de donner les permissions d'accès à toutes les ressources du corpus à un ou plusieurs utilisateurs ou à un ou plusieurs groupes d'utilisateurs.

Exemples:
- un professeur gère un corpus pour une ou plusieurs classes
- un responsable de corpus dans l'équipe algomus, peut donner des permissions sur le corpus à tout membre de l'équipe.

###### 3.3.2.1.1 - gestion des d'utilisteurs

Un adminstrateur de corpus peut ajouter un utilisateur (invitation) au corpus. Il ne peut pas créer de compte utilisteur. Seul l'administrateur principal peut créer les comptes utilisateurs.

Par contre un administrateur peut créer et gérer des groupes d'utilisateurs. Ex: le professeur aura besoin de gérer sa ou ses classes.

#### 3.3.3 propriétaire (non implémenté)

## 4 - Permissions

### 4.1 - Pour chaque ressource

Chaque ressource peut être accédée de 3 manières différentes:
- en lecture: READ -> read-corpus, read-piece, read-analysis
- en modification: UPDATE -> update-corpus, update-piece, update-analysis
- en supression: DELETE -> delete-corpus, delete-piece, delete-analysis
- juste pour la traversée: X

### 4.2 - Relations entre permission

#### 4.2.1 - Sur la même ressource

Pour une ressource donnée si un sujet a l'accès:
- CREATE alors il a l'accès DELETE
- DELETE alors il a l'accès UPDATE
- UPDATE alors il a l'accès READ

TODO: à implémenter.

#### 4.2.2 - Hierarchie déscendante (droit X)

Pour les ressources contenantes qui doivent être traversées pour accéder à une ressource enfant. La ressource contenante n'est pas 'lisible' ou 'listable' (droit READ) mais juste traversable.

Ainsi, si un sujet a l'accès READ à:
- une **pièce** alors il a l'accès X au **corpus** contenant
- un **sous-corpus** alors il a l'accès X au **corpus** contenant
- une **analyse** alors il a l'accès X à la **pièce** s'y rapportant

MAIS, le droit READ sur une ressource contenue n'entraine pas le droit READ sur la ressource contenante, seulement le droit X.

Remarque sur le stockage: cette permission n'est pas présente explicitement dans les fichiers `access.json` mais calculée par déduction de l'arbre de permissions.

#### 4.2.3 - Hierarchie ascendante

Si un sujet a l'accès READ à un **corpus** alors il a l'accès READ à tou(te)s les:
- **metadonnées** du corpus
- **pièces** du corpus (sauf exception)
- **sous-corpus** du corpus (sauf exception)

Si un sujet a l'accès UPDATE à un **corpus** alors il a l'accès UPDATE à tou(te)s les:
- **metadonnées** du corpus
- **pièces** du corpus (sauf exception)
- **sous-corpus** du corpus (sauf exception)

Si un sujet a l'accès DELETE à un **corpus** alors il peut le supprimer SAUF si il n'a pas l'accès DELETE à une des ressource filles (sous-corpus, pièces, analyses, images ou audios).

Si un sujet a l'accès CREATE à un **corpus** alors:
- il peut créer:
  - une pièce dans ce corpus
  - une pièce dans tous sous-corpus (sauf exception)
  - un sous-corpus dans ce corpus
  - un sous-corpus dans tous sous-corpus (sauf exception)
- il a l'accès CREATE à toutes les pièces et sous-corpus (sauf exception)

Si un sujet a l'accès READ à une **pièce** alors il a l'accès READ à toutes les:
- **metadonnées** de la pièce
- **images** de la pièce (sauf exception)
- **audios** de la pièce (sauf exception)
- **analyses** de la pièce (sauf exception)

Si un sujet a l'accès UPDATE à une **pièce** alors il a l'accès UPDATE à toutes les:
- **metadonnées** de la pièce
- **images** de la pièce (sauf exception)
- **audios** de la pièce (sauf exception)
- **analyses** de la pièce (sauf exception)

Si un sujet a l'accès DELETE à une **pièce** alors il peut la supprimer SAUF si il n'a pas l'accès DELETE à une des ressource filles (analyses, images ou audios).

Si un sujet a l'accès CREATE à une **pièce** alors:
- il peut créer:
  - une analyse de cette pièce
  - une images de cette pièce
  - une audio de cette pièce

### 4.3 - Pour les ressources contenantes (partiellemnt implémenté)

Les ressources contenantes (noeuds) peuvent également être accédées en creation (CREATE -> create-corpus, create-piece, create-analysis) des ressources qu'elles peuvent contenir. Ainsi pour qu'un sujet puisse créer l'analyse d'une pièce il doit avoir le droit 'create-analysis' sur la pièce.

Du fait du mécanisme d'heritage des permissions, pour que le sujet puisse le faire sur tout un corpus, il doit avoir la permission 'create-analysis' au niveau du corpus.

### 4.4 - Heritage des permissions

Les permissions peuvent être définies au niveau de la ressources ou au niveau de l'un de ses ancêtres par un mécanisme d'héritage. Autrement dit, si un sujet possède une permission au niveau d'un corpus, il la possède également pour l'ensemble des ressources sous ce corpus. Par exemple, si la permission 'update-analysis' est définie au niveau du corpus pour un sujet, le sujet pourra modifier toutes les analyses de toutes les pièces contenues dans le corpus et dans un de ses sous-corpus.

#### 4.4.1 - Exceptions dans l'héritage

Il est possible de 'casser' une règle d'héritage, positive ou négative (ajout du préfixe `no-` à la permission. ie: `no-read`) à un enfant du corpus en assignant la permission inverse à cet enfant pour le sujet.

### 4.5 - Permissions des ressources contenues sur le noeud (update-analysis sur un corpus ou une pièce)

Sur une ressource contenante accessible en READ, il est possible d'ajouter des permissions plus fortes pour les ressources filles.

Par exemple, à une pièce en accés READ, il est possible d'ajouter la permission de créer des analyses en ajoutant au niveau de la pièce la permission 'create-analysis'. Cette permission peut être mise au niveau du corpus le cas échéant.

### 4.6 - permissions GRANT (non implémenté)

Les permissions GRANT donnent le pouvoir à qui les possèdent de donner des permissions. Elles se déclinent selon les différents types d'accès vus précédemment: GRANT-CREATE, GRANT-READ, GRAND-UPDATE et GRANT-DELETE. Elles utilisent les mêmes règles d'heritages que les autres permissions.

Ce type de permission considère un sujet suceptible de recevoir la permission. Sur une ressource un sujet a la permission de donner une permission donnée (CRUD) à un sujet bien délimité.

Exemples:
- un professeur qui donne le droit READ sur une pièce à sa classe
- un utilisteur qui rend publique une pièce ou une analyse
- un utilistaur qui partage une analyse à un autre

### 4.7 - Permissions implementées (peuvent encore evoluer)

#### 4.7.1 - read

Sur un corpus: le sujet a accès en lecture à toutes les pièces et analyses du corpus et sous-corpus sauf exception. Il peut lister les fils du corpus (pièces et sous-corpus) sauf exception (si un fils a la permission no-read, il ne figurera pas dans la liste)

Sur une pièce: le sujet a accès en lecture à toutes les analysis de la pièce sauf exception. Il peut lister les analyses de la pièce sauf exception (si une analyse a la permission no-read, elle ne figurera pas dans la liste)

Sur une analyse: le sujet a accès en lecture à l'analyse.

#### 4.7.2 - update-analysis

L'implémentation actuelle de cette permission comprend également le droit de création d'analyse (CREATE).

Sur un corpus: le sujet peut créer ou modifier une analyse pour toutes les pièces du corpus et sous-corpus sauf exception.

Sur une pièce: le sujet peut créer une analyse pour la pièce. Il peut modifier toutes les analyses de la pièce sauf exception.

La permission update sur une analyse sous-entend la permission 'read' sur la pièce.

#### 4.7.3 - update-synchro

Sur un corpus: le sujet peut modifier la synchro de toutes les audios de toutes les pièces du courpus ou sous-corpus sauf exception.

Sur une pièce: le sujet peut modifier la synchro de toutes les audio de la pièce sauf exception.

Sur un audio: le sujet peut modifier la synchro de l'audio.

#### 4.7.4 - read-analysis

Sur un corpus: le sujet peut lire toutes les anlyses de toutes les pièces du corpus et de ses sous-corpus sauf exception.

Sur une pièce: le sujet peut lire toutes les analyses de la pièces sauf exception.

#### 4.7.5 - grant-read-analysis (à ré-implementer)

## 5 - Cas d'utilisation

- un sujet ne peut lister que les pièces d'un corpus sur lesquelles il a l'accès READ
- un sujet ne peut lister que les sous-corpus d'un corpus sur lesquels il a l'accés READ
- un sujet ne peut lister que les analyses d'une pièce sur lesquelles il a l'accès READ
- un sujet ne peut lister que les audios d'une pièce sur lesquelles il a l'accès READ
- un sujet ne peut lister que les images d'une pièce sur lesquelles il a l'accès READ

- donner l'accès READ à une pièce à un sujet qui n'a pas le droit READ 

## 6 - API rest

## 7 - Implémentation grâce à une hierarvhie de répertoires et de fichiers
