
# Credits

Dezrann is open-source (GPL v3+) and is developped by Emmanuel Leguy and the [Algomus](http://www.algomus.fr) computer music team. In addition to the core developers, we thank the many people who contribute to Dezrann across various aspects. Contributions span development, corpus creation and maintenance, teaching material creation, synchronization, documentation, translation, as well as providing valuable usage reports, bug reports, and suggestions. See also [codemeta.json](https://gitlab.com/algomus.fr/dezrann/dezrann/-/blob/dev/codemeta.json?ref_type=heads#L82).

## Core authors/developers

- Emmanuel Leguy, 2017-2025
- Mathieu Giraud, 2017-2025
- Lou Garczynski, 2021-2022
- Charles Ballester, 2022-2024

## Contributors

- Baptiste Bacot (usage, synchro)
- Louis Bigo (usage, corpus Mozart-piano-sonatas)
- Vanessa Nina Borsan (usage, corpus SLP, synchro, translation sl)
- Louis Couturier (usage, corpus Mozart-piano-sonatas, synchro)
- Ken Déguernel (usage, synchro)
- Théo de Pinho (usage, corpus Lute)
- Quentin Dinel (internship dev, corpus SWD, WJD)
- Félix Emery (usage, teaching materials)
- Laurent Feisthauer (usage, corpus Mozart-string-quartets, synchro)
- Mark Gotham (usage, corpus OpenScore-Lieder)
- Richard Groult (usage)
- Johannes Hentschel (usage, corpus Mozart-piano-sonata, translation de)
- Alexandre d'Hooge (usage, synchro)
- Dinh-Viet-Toan Le (usage, corpus Orchestration, synchro)
- Florence Levé (usage, corpus Mozart-piano-sonatas, synchro)
- Ling Ma (internship dev, 2018)
- Francesco Maccarini (usage, corpus Orchestration, synchro, translation it)
- Ivana Maričići (translation hr)
- Gianluca Micchi (usage, corpus Beethoven-piano-sonatas)
- Thomas Obry (internship dev, 2022)
- Nathan Ogueton (internship dev, 2022)
- Jérémie Roux (usage, corpus Corelli-trio-sonatas)
- Alice Sauda (usage, teaching materials)
- Alexandros Stamatiadis (translation el)
- Tom Taffin (internship dev, 2023)
- Patrice Thibaud (usage, synchro, corpus Telemann-flute-fantasias)
- Louison Wouts (usage, teaching materials)

## Special thanks

We express our gratitude to all the children and their teachers in the Académies de Lille et d'Amiens who participated in music lessons with Dezrann. Their active involvement not only contributed valuable usage and bug reports but also provided insightful suggestions. We extend our thanks to everyone involved in structuring these pedagogical activities around Dezrann.

- Emanuele Battisti
- Murielle Chaignier
- Camille De Visscher
- François Degroote
- Célia Delcroix
- Marie Gandin
- Guillaume Girin
- Rémi Gravelin
- Pauline Leroy
- Vincent Louette
- Laurent Raymond
- Anne-Isabelle Ramanantsitohaina
- Eulalie Tison

## Funding

- Université de Lille, [Les Sciences Infusent](https://sciencesinfusent.univ-lille.fr/), 2020-24
- CPER MAuVE, 2020-2022
- ANR CollabScore, 2022-2025

## References

- Charles Ballester et al.,
  *Interacting with Annotated and Synchronized Music Corpora on the Dezrann Web Platform*,
  Transactions of the International Society for Music Information Retrieval, in revision (2025)

- Mark Gotham, Johannes Hentschel, Louis Couturier, Nathan Dykeaylen, Martin Rohrmeier, Mathieu Giraud,
  [The 'Measure Map': an inter-operable standard for aligning symbolic music](https://hal.science/hal-04197400),
  Digital Librairies for Musicology (DLfM 2023), 2023

- Lou Garczynski, Mathieu Giraud, Emmanuel Leguy, Philippe Rigaux,
  [Modeling and editing cross-modal synchronization on a label web canvas](https://hal.archives-ouvertes.fr/hal-03583179),
  Music Encoding Conference (MEC 2022), 2022

- Ling Ma, Mathieu Giraud, Emmanuel Leguy,
  [Realtime collaborative annotation of music scores with Dezrann](https://hal.archives-ouvertes.fr/hal-02162935),
  Int. Symp. on Computer Music Multidisciplinary Research (CMMR 2019), 2019.

- Mathieu Giraud, Richard Groult, Emmanuel Leguy,
  [Dezrann, a Web Framework to Share Music Analysis](https://hal.archives-ouvertes.fr/hal-01796787v1),
  Int. Conf. on Technologies for Music Notation and Representation ([TENOR 2018](http://matralab.hexagram.ca/tenor2018/)), 2018  